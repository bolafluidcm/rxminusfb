import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import {Toast, ToasterConfig, ToasterService, BodyOutputType } from 'angular2-toaster';
import { NewsService } from './news/news.service';
import { JsonParseService } from '../../services/json-parse.service';
import { environment } from '../../../environments/environment'
import { Competition } from '../competition';
import { AnonymousSubscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-toaster-root',
  template: `
    <toaster-container [toasterconfig]="toasterconfig"><div class="omomi"></div>
    </toaster-container>`
})

export class ToastComponent implements OnInit, AfterViewInit, OnDestroy {
  private currentEvent = environment.currentEvent;
  private heatsSubscription: AnonymousSubscription;
  private toasterService: ToasterService;
  public toasterconfig: ToasterConfig =
    new ToasterConfig({
      limit: 1,
      showCloseButton: true,
      tapToDismiss: false,
      timeout: 600000,
      closeHtml: '<i class="fa fa-times-circle" aria-hidden="true"></i>',
      bodyOutputType: BodyOutputType.TrustedHtml
    });
  newsTitle: any;
  newsBody: any;
  newsUrl: any;
  news: any;
  competition: Competition;
  keyword;
  constructor( toasterService: ToasterService, private newsService: NewsService) {
    this.toasterService = toasterService;
  }
  ngOnInit() {
    this.getBreakingNews();
  }
  ngAfterViewInit() {
  }
  getBreakingNews() {
    this.heatsSubscription = this.newsService.getBreakingNews()
          .subscribe( news => {
              this.news = news;
              this.newsTitle = this.news[0].title;
              this.newsBody = this.news[0].content;
              this.newsUrl = this.news[0].id;
              const toast: Toast = {
                type: 'success',
                title: 'Breaking News',
                // showCloseButton: true,
                // body: '<a href="#/news/' + this.newsUrl + '">' + this.newsTitle + '</a>',
                body: this.newsTitle + ' : ' + '<span>' + this.newsBody + '</span>',
                // bodyOutputType: BodyOutputType.TrustedHtml
              };
              this.toasterService.pop(toast);
            }
          );
  }
  public ngOnDestroy(): void {
    if (this.heatsSubscription) {
      this.heatsSubscription.unsubscribe();
    }
  }
}
