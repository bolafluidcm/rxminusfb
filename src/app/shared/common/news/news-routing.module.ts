import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NewsComponent } from './news.component';
import { NewsListComponent } from './news-list/news-list.component';
import { NewsItemComponent } from './news-list/news-item/news-item.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: '/news',
    data: {
      title: 'news'
    },
    children: [
      {
        path: 'news',
        component: NewsListComponent,
        data: {
          title: 'news-list'
        }
      },
      {
        path: 'news/:id',
        component: NewsItemComponent,
        data: {
          title: 'news-item'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewsRoutingModule {}
