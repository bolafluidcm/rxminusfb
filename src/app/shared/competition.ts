export class Competition {
  constructor(
    public shortname: string,
    public round: number,
    public country: string,
    public location: string
  ) {

  }
}
