import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { JsonParseService } from '../../../app/services/json-parse.service';
import { SundayEveningComponent } from './sunday-evening.component';
import { SeVideosComponent } from './videos/se-videos.component';
import { SeTopVideosComponent } from './videos/se-top-videos.component';
import { SwiperModule } from 'angular2-useful-swiper';
import { SundayEveningRoutingModule } from './sunday-evening-routing.module';
import { SharedModule } from '../../../app/shared/shared.module';
import { NgPipesModule } from 'ngx-pipes';
import { FullLayoutComponent } from './components/full-layout.component';

@NgModule({
  imports:      [
    CommonModule,
    FormsModule,
    SundayEveningRoutingModule,
    SharedModule,
    NgPipesModule,
    SwiperModule
  ],
  declarations: [ SundayEveningComponent, SeVideosComponent, SeTopVideosComponent, FullLayoutComponent
  ],
  providers:    [ JsonParseService ]
})
export class SundayEveningModule { }





