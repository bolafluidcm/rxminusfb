import { Injectable } from '@angular/core';
import { Http, Response, Headers  } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { News } from './news';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { environment } from '../../../../environments/environment'

@Injectable()
export class NewsService {
  public news: News;
  private apiUrl = environment.apiEndpoint;
  private newsEndpoint = environment.newsUrl;
  private newsUrl = this.apiUrl + this.newsEndpoint;
  private breakingNewsInterval = environment.breakingNewsInterval;
  sundayEveningNewsUrl = environment.sundayEveningNewsUrl;
  sundayEvevningNewsCat = environment.sundayEveningNewsCat;
  sundayEveningNewsKeyword= environment.sundayEveningNewsKeyword;
  breakingNews = environment.breakingNews;
  constructor (private http: Http) {}
  getSundayEveningNews(): Observable<News[]> {
    return this.http.get(this.apiUrl + this.sundayEveningNewsUrl + '/' + this.sundayEvevningNewsCat + '/' + this.sundayEveningNewsKeyword)
        .map(response => response.json() as News[])
        .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
  getNewsItemDetail(id): Observable<any> {
    const url = this.newsUrl + '/' + id;
    return this.http.get(url)
      .map(res => {
        const newsItem = res.json();
        return newsItem;
      })
  }
  getCurrentNews(): Observable<News[]> {
    return this.http.get(this.newsUrl)
      .map(response => response.json() as News[])
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
  getBreakingNews(): Observable<News> {
    return Observable.interval(this.breakingNewsInterval)
      .switchMap(() => this.http.get(this.apiUrl + this.breakingNews))
      .map(response => response.json() as News)
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
  // getForBreakingNews(currEvent) {
  //   return Observable.interval(this.breakingNewsInterval)
  //     .switchMap(() => this.http.get(this.apiUrl + 'event' + '/' + currEvent))
  //     .map((res: Response) => res.json())
  //     .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  // }
  getSelectedNews(id): Observable<News> {
    return this.http.get(this.newsUrl + '/' + id)
      .do( res => console.log('HTTP response:', res) )
      .map(response => response.json() as News)
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
  get(id): Observable<News> {
    const news$ = this.http
      .get(this.newsUrl + '/' + id, {headers: this.getHeaders()})
      .map(mapNews);
      return news$;
  }
    private getHeaders() {
     const headers = new Headers();
     headers.append('Accept', 'application/json');
     return headers;
   }

}

  function mapNews(response: Response): News {
   return toNews(response.json());
  }
  function toNews(r: any): News {
   const news = <News>({
     id: r.id,
     title: r.title,
     content: r.content,
     image: r.image,
     url: r.url,
   });
     console.log('Parsed news:', news);
   return news;
  }



