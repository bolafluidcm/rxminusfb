import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { NewsComponent } from './news.component';
// import { NewsListComponent } from './news-list/news-list.component';
// import { NewsItemComponent } from './news-list/news-item/news-item.component';
import { NewsRoutingModule } from './news-routing.module';
import { SharedModule } from 'app/shared/shared.module';

@NgModule({
  imports: [
  CommonModule,
  NewsRoutingModule,
  SharedModule
  ],
  declarations: [
    // NewsComponent,
    // NewsListComponent,
    // NewsItemComponent
  ]
})
export class NewsModule { }
