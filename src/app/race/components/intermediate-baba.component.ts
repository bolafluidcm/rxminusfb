import {Component, OnInit, OnDestroy } from '@angular/core';
import { JsonParseService } from '../../services/json-parse.service';
import * as _ from 'lodash';
import { IntermediateBaseComponent } from '../../shared/basecomponent/intermediate-base.component';
import { Router } from '@angular/router';
import { AnonymousSubscription } from 'rxjs/Subscription';
@Component({
    selector: 'app-intermediate-baba',
    templateUrl: '../../../assets/templates/intermediate.html'
})
export class IntermediateBabaComponent extends IntermediateBaseComponent implements OnInit, OnDestroy {
    items: any[] = [];
    catName: any;
    counter: any;
    lastItem: number;
    currentParentRoute: string;
    private interSubscription: AnonymousSubscription;
    constructor(private service: JsonParseService, private router: Router) {
        super();
        this.router.events.subscribe((res) => {
            this.currentParentRoute = this.router.url.split('/')[2];
            this.catName = this.currentParentRoute;
            this.getAllIntersDupe(this.catName );
        })
    }
    getAllIntersDupe(catName) {
        this.service.getIntermediatesDupe(catName)
            .take(1)
            .subscribe( p => this.items = p );
    }
    countIntermediates(catName) {
        this.interSubscription = this.service.countIntermediates(catName)
            .subscribe( data => {
                this.counter = data;
                this.lastItem = 0;
                this.lastItem = _.parseInt(_.size(this.items));
                this.counter = _.parseInt(this.counter);
                if (this.lastItem < this.counter) {
                    this.getAllIntersDupe(catName);
                }else {
                    this.items = this.items;
                }
            });
    }
    ngOnInit() {
        this.countIntermediates(this.catName );
    }
    public ngOnDestroy(): void {
        if (this.interSubscription) {
            this.interSubscription.unsubscribe();
        }
    }
}
