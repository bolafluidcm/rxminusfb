import { Component, OnInit } from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-live-timings',
  templateUrl: './live-timings.component.html',
  styleUrls: ['./live-timings.component.scss']
})
export class LiveTimingsComponent implements OnInit {
  baseUrl = 'http://www.chronomoto.hu/livetiming/rx/';
  url: any;

  constructor(private sanitizer: DomSanitizer) {
    this.url = this.sanitizer.bypassSecurityTrustResourceUrl(this.baseUrl);
  }

  ngOnInit() {
  }

}
