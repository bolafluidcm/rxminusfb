import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SundayEveningComponent } from './sunday-evening.component';
import { FullLayoutComponent } from './components/full-layout.component';
import { IntermediatePageComponent } from '../../shared/common/pages/components/intermediate-page.component';
import { ChampionshipsPageComponent } from '../../shared/common/pages/components/championships-page.component';
import { EntrantsPageComponent } from '../../shared/common/pages/components/entrants-page.component';
import { SupercarMainComponent } from '../../race/supercar/supercar.component';
import { Rx2MainComponent } from '../../race/rx2/rx2.component';
import { ErxMainComponent } from '../../race/erx/erx.component';
import { Super1600MainComponent } from '../../race/super1600/super1600.component';
import { TouringcarMainComponent } from '../../race/touringcar/touringcar.component';
import { RaceDocumentsComponent } from '../../race/race-documents.component';
import { SchedulePageComponent } from '../../shared/common/schedule/schedule-page.component';
import {LiveTimingsComponent} from '../../shared/common/live-timings/live-timings.component';


const routes: Routes = [
  { path: '',
    component: SundayEveningComponent,
    data: {title: 'Sunday Evening'},
    children: [
      {
        path: '',
        redirectTo: 'supercar',
        component: SupercarMainComponent
      },
      {
        path: 'supercar',
        component: SupercarMainComponent
      },
      {
        path: 'supercar/intermediate_points',
        component: IntermediatePageComponent
      },
      {
        path: 'supercar/championship_points',
        component: ChampionshipsPageComponent
      },
      {
        path: 'supercar/entrants',
        component: EntrantsPageComponent
      },
      {
        path: 'supercar/schedule_of_events',
        component: SchedulePageComponent
      },
      {
        path: 'supercar/live_timings',
        component: LiveTimingsComponent
      },
      {
        path: 'supercar/race_documents',
        component: RaceDocumentsComponent
      },

      {
        path: 'rx2',
        component: Rx2MainComponent
      },
      {
        path: 'rx2/intermediate_points',
        component: IntermediatePageComponent
      },
      {
        path: 'rx2/championship_points',
        component: ChampionshipsPageComponent
      },
      {
        path: 'rx2/entrants',
        component: EntrantsPageComponent
      },
      {
        path: 'rx2/schedule_of_events',
        component: SchedulePageComponent
      },
      {
        path: 'rx2/race_documents',
        component: RaceDocumentsComponent
      },
      {
        path: 'rx2/live_timings',
        component: LiveTimingsComponent
      },

      {
        path: 'supercar_erx',
        component: ErxMainComponent
      },
      {
        path: 'supercar_erx/intermediate_points',
        component: IntermediatePageComponent
      },
      {
        path: 'supercar_erx/championship_points',
        component: ChampionshipsPageComponent
      },
      {
        path: 'supercar_erx/entrants',
        component: EntrantsPageComponent
      },
      {
        path: 'supercar_erx/schedule_of_events',
        component: SchedulePageComponent
      },
      {
        path: 'supercar_erx/race_documents',
        component: RaceDocumentsComponent
      },
      {
        path: 'supercar-erx/live_timings',
        component: LiveTimingsComponent
      },

      {
        path: 'super1600',
        component: Super1600MainComponent
      },
      {
        path: 'super1600/intermediate_points',
        component: IntermediatePageComponent
      },
      {
        path: 'super1600/championship_points',
        component: ChampionshipsPageComponent
      },
      {
        path: 'super1600/entrants',
        component: EntrantsPageComponent
      },
      {
        path: 'super1600/schedule_of_events',
        component: SchedulePageComponent
      },
      {
        path: 'super1600/race_documents',
        component: RaceDocumentsComponent
      },
      {
        path: 'super1600/live_timings',
        component: LiveTimingsComponent
      },

      {
        path: 'touringcar',
        component: Super1600MainComponent
      },
      {
        path: 'touringcar/intermediate_points',
        component: IntermediatePageComponent
      },
      {
        path: 'touringcar/championship_points',
        component: ChampionshipsPageComponent
      },
      {
        path: 'touringcar/entrants',
        component: EntrantsPageComponent
      },
      {
        path: 'touringcar/schedule_of_events',
        component: SchedulePageComponent
      },
      {
        path: 'touringcar/race_documents',
        component: RaceDocumentsComponent
      },
      {
        path: 'touringcar/live_timings',
        component: LiveTimingsComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SundayEveningRoutingModule {}
