import { Component, OnInit, OnDestroy } from '@angular/core';
import { ScheduleService } from './schedule.service';
import { Router } from '@angular/router';
import { Schedule } from './schedule';
import { JsonParseService } from '../../../services/json-parse.service';
import * as _ from 'lodash';
@Component({
  selector: 'app-schedule-page',
  templateUrl: './schedule-page.component.html',
  providers: [ ScheduleService ]
})
export class SchedulePageComponent implements OnInit {
    errorMessage: string;
    schedules: Schedule;
    items: any;
    mode = 'Observable';
    currentEvent:  any[];
    interval;
    now = new Date();
    asiko: string;
    selectedIndex: number;
    index = 9;

    constructor(private scheduleService: ScheduleService, private _router: Router,  private service: JsonParseService ) {
        this.interval = setInterval(() => {
            this.now = new Date() ;
        });
      // console.log('today is ' + this.dayNumber);
        this.selectedIndex = 0;

        const currentTime = new Date().toTimeString().substr(0, 8);
        this.asiko = new Date().toTimeString().substr(0, 8);
        // console.log('time na ' + this.asiko);
        // console.log('Index mi nko ' + this.selectedIndex);
      // this.dateString = this.now > this.schedules.theTime;
        this.getEvent();
    }

    ngOnInit() {
        this.getSchedules();
        // this.myslider.slideTo(9,0,false);

        // console.log('schedule time is ' + this.schedules.theTime);
    }
    // ngOnDestroy() {
    //     clearInterval(this.interval);
    // }

    getSchedules() {
        this.scheduleService.getSchedulesPage()
            .subscribe(
                schedules => this.schedules = schedules,
                error =>  this.errorMessage = <any>error);

    }

    next() {
        ++this.selectedIndex;
    }

    previous() {
        --this.selectedIndex;
    }
    getEvent() {
        // this.service.getEvent()
        //     .subscribe(
        //         currentEvent => this.currentEvent = currentEvent,
        //         error =>  this.errorMessage = <any>error);

      this.scheduleService.getSchedulesPage().subscribe(
        data => {
          const grouped = _.groupBy(data, function (d) {
            return 'Day ' + d.day;
          });
          this.items = grouped;
        });
    }
}
