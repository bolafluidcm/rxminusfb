import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { HttpModule, JsonpModule } from '@angular/http';
import { JsonParseService } from './services/json-parse.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { NAV_DROPDOWN_DIRECTIVES } from './shared/directives/nav-dropdown.directive';
import { SwiperModule } from 'angular2-useful-swiper';
import {ToasterModule, ToasterService} from 'angular2-toaster';
import { SIDEBAR_TOGGLE_DIRECTIVES } from './shared/directives/sidebar.directive';
import { AsideToggleDirective } from './shared/directives/aside.directive';
import { BreadcrumbsComponent } from './layouts/breadcrumb.component';
import { AppRoutingModule } from './app.routing';
import { AuthGuardService } from './services/auth-guard.service';
import { FullLayoutComponent } from './layouts/full-layout.component';
import { SharedModule } from './shared/shared.module';
import {ToastComponent} from './shared/common/toast.component';

@NgModule({
  imports: [
    BrowserModule,
    AppRoutingModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    JsonpModule,
    HttpModule,
    BrowserAnimationsModule,
    SharedModule,
    FormsModule,
    SwiperModule,
    ToasterModule
  ],
  declarations: [
    AppComponent,
    FullLayoutComponent,
    NAV_DROPDOWN_DIRECTIVES,
    BreadcrumbsComponent,
    SIDEBAR_TOGGLE_DIRECTIVES,
    AsideToggleDirective,
    ToastComponent
  ],
  providers: [
    AuthGuardService,
    JsonParseService, {
    provide: LocationStrategy,
    useClass: HashLocationStrategy,
  },
    ToasterService
     ],
  bootstrap: [ AppComponent ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class AppModule { }
