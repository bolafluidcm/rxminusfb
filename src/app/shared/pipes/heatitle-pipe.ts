import { PipeTransform, Pipe } from '@angular/core';

@Pipe({name: 'heatitle'})
export class HeatitlePipe implements PipeTransform {
    transform(value: string, args: string) : string {
    let limit = args ? parseInt(args, 20) : 20;
    let trail = '';

    return value.length > limit ? value.substring(limit, 0 ) + trail : value;
  }
}
//  transform(value: string, limit: number, trail: string, position: string): string {
//    value = value || '';  // handle undefined/null value
//    limit = limit || 10;
//    trail = trail || '...';
//    position = position || 'right';
//
//    if (position === 'left') {
//      return value.length > limit
//        ? trail + value.substring(value.length - limit, value.length)
//        : value;
//    } else if (position === 'right') {
//      return value.length > limit
//        ? value.substring(0, limit) + trail
//        : value;
//    } else if (position === 'middle') {
//      return value.length > limit
//        ? value.substring(0, limit/2) + trail + value.substring(value.length - limit/2, value.length)
//        : value;
//    } else {
//      return value;
//    }
//  }
//}