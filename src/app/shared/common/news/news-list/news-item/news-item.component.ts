import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NewsService } from '../../news.service';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SafehtmlPipe } from '../../../../pipes/safehtml-pipe';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-news-item',
  templateUrl: '../../../../../../assets/templates/news-item.html',
  providers: [ NewsService ]
})
export class NewsItemComponent implements OnInit {
	sub: any;

	public newsItem;
  name: string;
  baseUrl:string = 'https://www.youtube.com/embed/';
  url:any;

  constructor(private newsService: NewsService, private activatedRoute: ActivatedRoute, private router: Router, private sanitizer: DomSanitizer) {
    // this.url = this.sanitizer.bypassSecurityTrustResourceUrl(this.baseUrl + this.newsItem.youtube);
  }

  ngOnInit() {
    const id = this.activatedRoute.snapshot.params['id'];
    console.log(id);
    this.newsService.getNewsItemDetail(id)
      //.subscribe(
      //  res =>{
      //    this.newsItem = res;
      //    console.log(this.newsItem);
      //  }

	      .subscribe(
        data => {
          const blogArray = [];
          for (let key in data) {
            blogArray.push(data[key]);
          }
          this.newsItem = blogArray;
        }

      )
//        this.sub = this.route.params.subscribe(params => {
//          let id = Number.parseInt(params['id']);
//          console.log('getting news with id: ', id);
//          this.newsService
//            .getSelectedNews(id)
//            .subscribe(p => this.news = p);
//						console.log('retrieved news: ', this.news);
//        });
//       this.news = this.route.snapshot.data['news'];
		}

  isEmptyObject(obj) {
    return (obj && (Object.keys(obj).length === 4));
  }


}

