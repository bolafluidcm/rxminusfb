import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
@Component({
  selector: 'app-intermediate-base',
  templateUrl: '../../../assets/templates/intermediate.html',
  styles: []
})
export class IntermediateBaseComponent implements OnInit {
  flagsUrl: any = environment.flagsUrl;
  manufacturersUrl: any  = environment.manufacturersUrl;
  carsUrls: any  = environment.carsUrls;
  carSmallSize: any  = environment.carSmallSize;
  carLargeSize: any  = environment.carLargeSize;
  show = 10;
  showMobile = 5;
  next  = 5;
  hide  = true;
  hideMobile  = true;
  data: Observable<Array<any>>;
  items: any[] = [];
  @ViewChild('loadMore') loadMore: ElementRef;

  constructor() {}
  showMore() {
    let event = new MouseEvent('click', {bubbles: true});
    event.stopPropagation();
    this.show = this.items.length;
    if (this.show >= this.items.length) {
      this.hide = false;
    }
    this.loadMore.nativeElement.dispatchEvent(event);
  }
  showMoreMobile() {
    let event = new MouseEvent('click', {bubbles: true});
    event.stopPropagation();
    this.show = this.items.length;
    if ( this.showMobile >= this.items.length) {
      this.hideMobile = false;
    }
    this.loadMore.nativeElement.click();
  }
  ngOnInit() {}
}
