import { Component, OnInit } from '@angular/core';
import { TweetsService } from './tweets.service';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';

@Component({
    selector: 'app-tweets',
    templateUrl: './tweets.component.html',
    providers: [ TweetsService ],
    styles: []
})
export class TweetsComponent implements OnInit {
    errorMessage: string;
    tweets: any[];
    mode = 'Observable';

    config: Object = {
        pagination: '.swiper-pagination',
        // nextButton: '.swiper-button-next',
        // prevButton: '.swiper-button-prev',
        spaceBetween: 10,
        slidesPerView: 4,
        paginationClickable: true,
        loop: false,
        breakpoints: {
            1279: {
                slidesPerView: 3,
                spaceBetween: 10
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 10
            },
            640: {
                slidesPerView: 2,
                spaceBetween: 10
            },
            320: {
                slidesPerView: 1,
                spaceBetween: 10
            }
        }
    };

    constructor(private tweetsService: TweetsService, private _sanitizer: DomSanitizer ) {
      this.getTweets();
    }

    ngOnInit() {}

    getTweets() {
        this.tweetsService.getTweets()
            .subscribe(
              tweets => this.tweets = tweets,
                error =>  this.errorMessage = <any>error);
    }

  getBackground(image) {
    return this._sanitizer.bypassSecurityTrustStyle(`url(${image})`);
  }

}
