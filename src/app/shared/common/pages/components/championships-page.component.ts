import {Component, Inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { JsonParseService } from '../../../../services/json-parse.service';
import { ChampionshipsBaseComponent } from '../../../basecomponent/championships-base.component';
@Component({
  moduleId: module.id,
  selector: 'app-championships-page',
  templateUrl: '../layouts/championships-page.html'
})
export class ChampionshipsPageComponent extends ChampionshipsBaseComponent implements OnInit {
  items: any[] = [];
  catName: any;
  currentParentRoute: string;
  constructor(@Inject(JsonParseService) service: JsonParseService, private router: Router) {
    super(service);
    this.router.events.subscribe((res) => {
      this.currentParentRoute = this.router.url.split('/')[2];
      this.catName = this.currentParentRoute;
      this.getAllChamps(this.catName );
    })
  }
  getAllChamps(catName) {
    this.service.getChamps(catName).subscribe( p => this.items = p );
  }
  ngOnInit() {}
}
