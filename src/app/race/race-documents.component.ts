import { Component, OnInit } from '@angular/core';
import {JsonParseService} from '../services/json-parse.service';
import {Router} from '@angular/router';
import {environment} from '../../environments/environment';
import _ from 'lodash';

@Component({
  selector: 'app-race-documents',
  templateUrl: './race-documents.component.html',
  styles: []
})
export class RaceDocumentsComponent implements OnInit {
  private resultpdf = environment.resultpdf;
  items: any[] = [];
  errorMessage: any;
  id: any;
  currentParentRoute: string;
  koko: any;
  constructor(private service: JsonParseService, private router: Router) {
  this.getDocuments();
  }

  ngOnInit() {
  }

  getDocuments() {
    this.router.events.subscribe(() => {
      this.currentParentRoute = this.router.url.split('/')[2];
      this.id = _.find(this.resultpdf, ['cat', this.currentParentRoute]);
      // console.log(this.id.pdfrel);
        const catid = this.id.pdfrel;
        this.service.getDocuments(catid)
          .subscribe(
            p => this.items = p,
            error =>  this.errorMessage = <any>error);
    });

  }

}
