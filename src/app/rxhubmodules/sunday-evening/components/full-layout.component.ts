import { Component, OnInit } from '@angular/core';
import { Routes, RouterModule, Router, ActivatedRoute } from '@angular/router';
import { Dropdown } from './dropdown';
import { JsonParseService } from '../../../services/json-parse.service'

@Component({
  selector: 'app-dashboard',
  templateUrl: '../layouts/full-layout.component.html'
})
export class FullLayoutComponent implements OnInit {

  public disabled = false;
  public status: {isopen: boolean} = {isopen: false};

  public toggled(open: boolean): void {
    console.log('Dropdown is now: ', open);
  }

  public toggleDropdown($event: MouseEvent): void {
    $event.preventDefault();
    $event.stopPropagation();
    this.status.isopen = !this.status.isopen;
  }

  // selectedDropdown: Dropdown = new Dropdown(1, 'SC', 'Supercar', 'home');
  errorMessage: string;
  currentEvent:  any[];
  selectedDropdown: Dropdown;
  dropdowns = [
  new Dropdown(1, 'SC', 'Supercar', 'home'),
  new Dropdown(2, 'S1600', 'Super1600', 'super1600'),
  new Dropdown(3, 'TC', 'Touringcar', 'touringcar'),
  new Dropdown(4, 'RX2', 'RX2', 'rx2'),
  new Dropdown(5, 'ERX', 'Supercar ERX', 'supercar_erx')
  ];
  selected: any;
  // levels:Array<Object> = [
  //   {num: 0, name: "AA"},
  //   {num: 1, name: "BB"}
  // ];

  constructor(private router: Router, private service: JsonParseService){
    this.router.events.subscribe((res) => {
      // console.log(this.router.url,"Current URL");
    })
  }

  ngOnInit(): void {
    this.selectedDropdown = this.dropdowns[0];
    // this.selected = this.levels[0];
    this.getEvent();
  }

  getEvent() {
  this.service.getEvent()
      .subscribe(
          currentEvent => this.currentEvent = currentEvent,
          error =>  this.errorMessage = <any>error);
}


  oninput($event){
    $event.preventDefault();
  }
  onSelect(){
    console.log('I am the selected' + this.selectedDropdown);
  }
  // getResult(event){
  //   this.router.navigate(['your_path', event])
  // }
  navigateTo(value) {
    if (value) {
      this.router.navigate([value]);
    }
    return false;
  }


}
