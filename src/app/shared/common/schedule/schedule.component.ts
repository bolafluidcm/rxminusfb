import { Component, OnInit, OnDestroy, ViewChild, Input } from '@angular/core';
import 'rxjs/add/observable/timer';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';
import { ScheduleService } from './schedule.service';
import { JsonParseService } from '../../../services/json-parse.service'
import { SwiperComponent } from 'angular2-useful-swiper';
import { Schedule } from './schedule';
import _ from 'lodash';

@Component({
    selector: 'app-schedule',
    templateUrl: './schedule.component.html',
    providers: [ ScheduleService ],
    styles: [],
})
export class ScheduleComponent implements OnInit {
    @ViewChild('usefulSwiper') usefulSwiper: SwiperComponent;
    @Input() src: string;
    1 = 'Friday';
    2 = 'Saturday';
    3 = 'Sunday';
    errorMessage: string;
    schedule: Schedule;
    schedules: Schedule[];
    index: number;
    currentEvent:  any[];
    config: any = {
      pagination: '.swiper-pagination',
      nextButton: '.swiper-button-next',
      prevButton: '.swiper-button-prev',
      spaceBetween: 0,
      slidesPerView: 12,
      paginationClickable: false,
      loop: false,
      observer: true,
      observeParents: true,
      breakpoints: {
          1440: {
            slidesPerView: 10,
            spaceBetween: 0
          },
          1280: {
            slidesPerView: 8,
            spaceBetween: 0
          },
          1024: {
              slidesPerView: 6,
              spaceBetween: 0
          },
          768: {
              slidesPerView: 6,
              spaceBetween: 0
          },
          737: {
              slidesPerView: 6,
              spaceBetween: 0
          },
          640: {
              slidesPerView: 4,
              spaceBetween: 0
          },
          320: {
              slidesPerView: 4,
              spaceBetween: 0
          }
      }
    };

  constructor(private scheduleService: ScheduleService, private service: JsonParseService) {
    this.scheduleService.getSchedules().subscribe((schedules) => {
      const currentTime = new Date().toLocaleTimeString('en-GB');
      this.schedules = schedules;
      this.index = _.parseInt(this.schedules.findIndex(x => x.theTime >= currentTime));
      this.index = this.index - 1;
      this.usefulSwiper.swiper.slideTo(this.index);
    }
      );
      this.getEvent();
  }

    ngOnInit() {
      // this.dataRefreshMonitor();
    }
    getEvent() {
      this.service.getEvent()
        .subscribe(
          currentEvent => this.currentEvent = currentEvent,
          error =>  this.errorMessage = <any>error);
    }

}
