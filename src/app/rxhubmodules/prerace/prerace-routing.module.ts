import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PreraceComponent } from './prerace.component';


const routes: Routes = [
    { path: '',
        component: PreraceComponent,
        data: {title: 'Prerace'}
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PreraceRoutingModule {}