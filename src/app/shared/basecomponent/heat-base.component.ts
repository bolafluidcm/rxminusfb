import {AfterViewInit, Component, OnInit, OnDestroy, ViewChild} from '@angular/core';
import { JsonParseService } from '../../services/json-parse.service';
import { environment } from '../../../environments/environment';
import {SwiperComponent} from 'angular2-useful-swiper';
@Component({
  selector: 'app-heat-base',
  templateUrl: '../../../assets/templates/supercarheats.html',
  styles: []
})
export class HeatBaseComponent implements OnInit, AfterViewInit, OnDestroy {
  flagsUrl: any = environment.flagsUrl;
  manufacturersUrl: any  = environment.manufacturersUrl;
  carsUrls: any  = environment.carsUrls;
  carSmallSize: any  = environment.carSmallSize;
  carLargeSize: any  = environment.carLargeSize;
  items: any[] = [];
  config: Object = {
    pagination: '.swiper-pagination',
    paginationClickable: true,
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    spaceBetween: 10,
    slidesPerView: 1,
    loop: false,
    observer: true,
    observeParents: true,
    breakpoints: {
      320: {
        slidesPerView: 1,
        spaceBetween: 10
      },
      480: {
        slidesPerView: 1,
        spaceBetween: 20
      },
      640: {
        slidesPerView: 1,
        spaceBetween: 30
      }
    }
  };
  @ViewChild('usefulSwiper') usefulSwiper: SwiperComponent;
  @ViewChild('usefulSwiperMobile') usefulSwiperMobile: SwiperComponent;
  constructor(public service: JsonParseService) {
  }

  ngOnInit() {}
  ngAfterViewInit() {}
  ngOnDestroy() {}
}
