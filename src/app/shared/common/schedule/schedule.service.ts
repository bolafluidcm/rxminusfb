import { Injectable } from '@angular/core';
import { Http, Response  } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { Schedule } from './schedule';
import { environment } from '../../../../environments/environment';

@Injectable()
export class ScheduleService {
  schedule: Schedule;
  public count = 0;
  private eventsUrl = environment.eventsUrl;
  constructor (private http: Http) {}
    get() {
        return this.count;
    }
    getSchedulesPage() {
        return this.http.get(this.eventsUrl)
            .map((response: Response) => response.json());
    }
    getSchedules(): Observable<Schedule[]> {
        return this.http.get(this.eventsUrl)
            .map((response: Response) => response.json());
    }
}




