import { Component, OnInit } from '@angular/core';
import { News } from '../news';
import { NewsService } from '../news.service';
import { Routes, RouterModule, Router } from '@angular/router';


@Component({
  selector: 'app-news-list',
  templateUrl: './news-list.component.html',
  providers: [ NewsService ],
  styles: []
})
export class NewsListComponent implements OnInit {
  errorMessage: string;
  news: any[];
  mode = 'Observable';
  kolobaba: string = 'motide';
  // router;



  config: Object = {
    pagination: '.swiper-pagination',
    // paginationClickable: true,
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    spaceBetween: 10,
    slidesPerView: 4,
    loop:true,
    breakpoints: {
      1024: {
        slidesPerView: 3,
        spaceBetween: 10
      },
      768: {
        slidesPerView: 2,
        spaceBetween: 10
      },
      640: {
        slidesPerView: 1,
        spaceBetween: 20
      },
      320: {
        slidesPerView: 1,
        spaceBetween: 10
      }
    }
  };

  constructor(private newsService: NewsService, private _router: Router ) { }

  ngOnInit() {
    this.getCurrentNews();
    }
  // ngOnInit() { this.getDrivers();  }

  // getDrivers() {
  //   this.driverService.getDrivers()
  //     .subscribe(
  //       drivers => this.drivers = drivers,
  //       error =>  this.errorMessage = <any>error);
  // }

  getCurrentNews() {
    this.newsService.getCurrentNews()
      .subscribe(
        news => this.news = news,
        error =>  this.errorMessage = <any>error);
  }

  showNewsItemDetail(newsItem){
    this._router.navigate(['/news', newsItem.id]);
  }
  //newsSelected(news: News){
  //  this.router.navigate(['/news', news.id]);
  //}

}
