import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import { environment } from '../../environments/environment';
import {Heat} from '../race/heat';
// import {IntervalObservable} from 'rxjs/observable/IntervalObservable';

@Injectable()
export class JsonParseService {
    private apiUrl = environment.apiEndpoint;
    private jsonUrl = environment.jsonEndpoint;
    private jsonHeats = environment.jsonHeats;
    private jsonPosition = environment.jsonPosition;
    private jsonChamps = environment.jsonChamps;
    private jsonInter = environment.jsonInter;
    private jsonInterCount = environment.jsonInterCount;
    private jsonEntrants = environment.jsonEntrants;
    private jsonCategories = environment.jsonCategories;
    private jsonCurrentevent = environment.jsonCurrentevent;
    private currentEvent = environment.currentEvent;
    private jsonEntrantsCount = environment.jsonEntrantsCount;
  private heatInterval = environment.heatInterval;
  private positionInterval = environment.positionInterval;
  private intermediateInterval = environment.intermediateInterval;
  private entrantsInterval = environment.entrantsInterval;
  heat: Heat[] = [];
  // private currentYear = environment.currentYear;
  // private heatsUrl = environment.heatsUrl;
  // private supercar = environment.supercar;
  // private super1600 = environment.super1600;
  // private touringcar = environment.touringcar;
  // private rx2 = environment.rx2;
  // private erx = environment.erx;
  // private entrantsUrl = environment.entrantsUrl;
  // uniqueUrl;
  // timeStamp = new Date().getTime();
    constructor(private http: Http) { }

    getAllHeats(catName) {
        return Observable.interval(this.heatInterval)
        .switchMap(() => this.http.get(this.jsonUrl + catName + '/' + this.jsonHeats))
        .map((response: Response) => response.json());
    }
    getAllHeatsDupe(catName) {
        return this.http.get(this.jsonUrl + catName + '/' + this.jsonHeats)
        .map((response: Response) => response.json());
    }

    getPositions(catName): Observable<any> {
        return Observable.interval(this.positionInterval)
            .switchMap(() => this.http.get(this.jsonUrl + catName + '/' + this.jsonPosition))
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }
    getPositionsDupe(catName): Observable<any> {
        return this.http.get(this.jsonUrl + catName + '/' + this.jsonPosition)
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }
// Intermediates
    getIntermediates(catName): Observable<any> {
        return Observable.interval(this.intermediateInterval)
            .switchMap(() => this.http.get(this.jsonUrl + catName + '/' + this.jsonInter))
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }
    getIntermediatesDupe(catName): Observable<any> {
        return this.http.get(this.jsonUrl + catName + '/' + this.jsonInter)
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }
    countIntermediates(catName): Observable<any> {
        return Observable.interval(this.intermediateInterval)
            .switchMap(() => this.http.get(this.jsonUrl + catName + '/' + this.jsonInterCount))
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }

// Championships
    getChamps(catName): Observable<any> {
        return this.http.get(this.jsonUrl + catName + '/' + this.jsonChamps)
            .map((res: Response) => res.json())
            // errors if any
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }
// Categories
//     getCat() {
//         return this.http.get(this.jsonUrl + this.jsonCategories)
//             .map((response: Response) => response.json());
//     }
// Event
    getEvent() {
        return this.http.get(this.jsonUrl + this.jsonCurrentevent)
            .map((response: Response) => response.json());
    }
  // getEventClass () {
  //   return this.http.get(this.apiUrl + 'eventclass' + '/77')
  //     .map((response: Response) => response.json());
  // }

// Entrants

    getEntrantsPage(catName): Observable<any> {
        return Observable.interval(this.entrantsInterval)
            .switchMap(() => this.http.get(this.jsonUrl + catName + '/' + this.jsonEntrants))
            .map((res: Response) => res.json())
            // errors if any
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }
    getEntrantsPageDup(catName): Observable<any> {
        return this.http.get(this.jsonUrl + catName + '/' + this.jsonEntrants)
            .map((res: Response) => res.json())
            // errors if any
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }
    countEntrants(catName): Observable<any> {
        return Observable.interval(this.intermediateInterval)
            .switchMap(() => this.http.get(this.jsonUrl + catName + '/' + this.jsonEntrantsCount))
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }
    // Documents
  getDocuments(catid): Observable<any> {
    return this.http.get(this.apiUrl + 'document/' + catid + '/' + this.currentEvent )
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

}
