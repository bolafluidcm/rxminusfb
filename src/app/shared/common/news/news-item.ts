export interface NewsItem {
  
    title: string;
    image: string;
    content: string;

}
