import { Component, OnInit } from '@angular/core';
// import { News } from '../news';
import { NewsService } from '../../shared/common/news/news.service';
import { Routes, RouterModule, Router } from '@angular/router';
import {DomSanitizer} from '@angular/platform-browser';


@Component({
  selector: 'app-preracenews-list',
  templateUrl: './prerace-news-list.component.html',
  styles: []
})
export class PreraceNewsListComponent implements OnInit {
  errorMessage: string;
  news: any[];
  mode = 'Observable';
  baseUrl:string = 'https://www.youtube.com/embed/';

  config: Object = {
    pagination: '.swiper-pagination',
    // paginationClickable: true,
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    spaceBetween: 10,
    slidesPerView: 4,
    loop:true,
    breakpoints: {
      1024: {
        slidesPerView: 3,
        spaceBetween: 10
      },
      768: {
        slidesPerView: 2,
        spaceBetween: 10
      },
      640: {
        slidesPerView: 1,
        spaceBetween: 20
      },
      320: {
        slidesPerView: 1,
        spaceBetween: 10
      }
    }
  };

  constructor(private newsService: NewsService, private _router: Router, private sanitizer: DomSanitizer ) { }

  ngOnInit() {
    this.getCurrentNews();
    }

  getCurrentNews() {
    this.newsService.getCurrentNews()
      .subscribe(
        news => this.news = news,
        error =>  this.errorMessage = <any>error);
  }

  // showNewsItemDetail(newsItem){
  //   this._router.navigate(['/news', newsItem.id]);
  // }
}
