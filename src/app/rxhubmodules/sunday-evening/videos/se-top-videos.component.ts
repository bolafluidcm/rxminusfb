import { Component, OnInit } from '@angular/core';
import { NewsService } from '../../../shared/common/news/news.service';
import { Routes, RouterModule, Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
    selector: 'app-setopvideos',
    templateUrl: './se-top-videos.component.html',
    styles: []
})
export class SeTopVideosComponent implements OnInit {
    errorMessage: string;
    news: any[];
    mode = 'Observable';
    baseUrl:string = 'https://www.youtube.com/embed/';

    config: Object = {
        pagination: '.swiper-pagination',
        paginationClickable: true,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        spaceBetween: 10,
        slidesPerView: 1,
        loop:false,
        breakpoints: {
            1024: {
                slidesPerView: 1,
                spaceBetween: 10
            },
            768: {
                slidesPerView: 1,
                spaceBetween: 10
            },
            640: {
                slidesPerView: 1,
                spaceBetween: 20
            },
            320: {
                slidesPerView: 1,
                spaceBetween: 10
            }
        }
    };

    constructor(private newsService: NewsService, private _router: Router, private sanitizer: DomSanitizer ) { }

    ngOnInit() {
        this.getSeNews();
    }

    getSeNews() {
        this.newsService.getSundayEveningNews()
            .subscribe(
                news => this.news = news,
                error =>  this.errorMessage = <any>error);
    }

    // showNewsItemDetail(newsItem){
    //   this._router.navigate(['/news', newsItem.id]);
    // }
}
