import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import $ from 'jquery';
import 'hammerjs';
import { SwiperModule } from 'angular2-useful-swiper';
// Pipes
import { NgPipesModule } from 'ngx-pipes';
import { GroupByPipe } from './pipes/group-by-pipe';
import { GroupsPipe } from './pipes/groups-pipe';
import { KeysPipe } from './pipes/keys-pipe';
import { ExplodePipe } from './pipes/explode-pipe';
import { TruncatePipe } from './pipes/truncate-pipe';
import { HeatitlePipe } from './pipes/heatitle-pipe';
import { OrdinalPipe } from './pipes/ordinal-pipe';
import { ExplodetimePipe } from './pipes/explodetime-pipe';
import { DateFormatPipe } from './pipes/date-format-pipe';
import { SafehtmlPipe } from './pipes/safehtml-pipe';
import { YoutubePipe } from './pipes/youtube-pipe';
// Common
import { NewsComponent } from './common/news/news.component';
import { NewsListComponent } from './common/news/news-list/news-list.component';
import { NewsItemComponent } from './common/news/news-list/news-item/news-item.component';
import { NewsService } from './common/news/news.service';
import { NewsResolve } from './common/news/news.resolve';
import { ScheduleComponent } from './common/schedule/schedule.component';
import { SchedulePageComponent } from './common/schedule/schedule-page.component';
import { TweetsComponent } from './common/tweets/tweets.component';
// Race
import { SupercarMainComponent } from '../race/supercar/supercar.component';
import { Rx2MainComponent } from '../race/rx2/rx2.component';
import { HeatBabaComponent } from '../race/components/heat-baba.component';
import { PositionsBabaComponent } from '../race/components/positions-baba.component';
import { ChampionshipsBabaComponent } from '../race/components/championships-baba.component';
import { IntermediateBabaComponent } from '../race/components/intermediate-baba.component';
import { ErxMainComponent } from '../race/erx/erx.component';
import { Super1600MainComponent } from '../race/super1600/super1600.component';
import { TouringcarMainComponent } from '../race/touringcar/touringcar.component';
import { RaceDocumentsComponent } from '../race/race-documents.component';
// Pages
import { IntermediatePageComponent } from './common/pages/components/intermediate-page.component';
import { ChampionshipsPageComponent } from './common/pages/components/championships-page.component';
import { EntrantsPageComponent } from './common/pages/components/entrants-page.component';
/*Base Components*/
import { PositionsBaseComponent } from './basecomponent/positions-base.component';
import { HeatBaseComponent } from './basecomponent/heat-base.component';
import { LiveTimingsComponent } from './common/live-timings/live-timings.component';
import { IntermediateBaseComponent } from './basecomponent/intermediate-base.component';
import { ChampionshipsBaseComponent } from './basecomponent/championships-base.component';
// Services
import { JsonParseService } from '../services/json-parse.service';
import { LoggingService } from '../services/logging.service';

import { PreraceComponent } from '../rxhubmodules/prerace/prerace.component';
import { PreraceNewsListComponent } from '../rxhubmodules/prerace/prerace-news-list.component';
import { PreraceShakedownComponent } from '../rxhubmodules/prerace/prerace-shakedown.component';
import {RaceComponent} from '../race/race.component';

@NgModule({
  imports:      [
    CommonModule,
    FormsModule,
    NgPipesModule,
    RouterModule,
    SwiperModule
  ],
  declarations: [ GroupByPipe, GroupsPipe, KeysPipe, ExplodePipe, TruncatePipe, YoutubePipe,
    PositionsBaseComponent, HeatBaseComponent, HeatitlePipe, OrdinalPipe,
    ExplodetimePipe, DateFormatPipe, SafehtmlPipe, RaceComponent,
    NewsComponent, NewsListComponent, NewsItemComponent, ScheduleComponent,
    SchedulePageComponent, TweetsComponent, PreraceComponent,
    PreraceNewsListComponent, PreraceShakedownComponent, RaceDocumentsComponent,
    SupercarMainComponent, Rx2MainComponent, IntermediatePageComponent, ChampionshipsPageComponent,
    EntrantsPageComponent, ErxMainComponent, Super1600MainComponent, TouringcarMainComponent,
    LiveTimingsComponent, IntermediateBaseComponent, ChampionshipsBaseComponent,
    HeatBabaComponent, PositionsBabaComponent, ChampionshipsBabaComponent, IntermediateBabaComponent
  ],
  providers:    [ JsonParseService, NewsService, NewsResolve,  LoggingService ],
  exports: [ GroupByPipe, GroupsPipe, KeysPipe, ExplodePipe, TruncatePipe, YoutubePipe,
    PositionsBaseComponent, HeatBaseComponent, HeatitlePipe, OrdinalPipe,
    ExplodetimePipe, DateFormatPipe, SafehtmlPipe, RaceComponent,
    NewsComponent, NewsListComponent, NewsItemComponent, ScheduleComponent,
    SchedulePageComponent, TweetsComponent, PreraceComponent,
    PreraceNewsListComponent, PreraceShakedownComponent, RaceDocumentsComponent,
    SupercarMainComponent, Rx2MainComponent, IntermediatePageComponent, ChampionshipsPageComponent,
    EntrantsPageComponent, ErxMainComponent, Super1600MainComponent, TouringcarMainComponent,
    LiveTimingsComponent, IntermediateBaseComponent, ChampionshipsBaseComponent,
    HeatBabaComponent, PositionsBabaComponent, ChampionshipsBabaComponent, IntermediateBabaComponent
  ]
})
export class SharedModule { }





