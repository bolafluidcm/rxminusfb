import { Injectable } from '@angular/core';
import { CanActivate, Router, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';

@Injectable()
export class AuthGuardService implements CanActivate {

    // you would usually put this in it's own service and not access it directly!
    // this is just for the sake of the demo.
    isLoggedIn: boolean = false;
    dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

    constructor( private router: Router ) {

    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const dayNumber = new Date().getDay();
        // console.log('today na' + dayNumber);
        // if (this.isLoggedIn) {
        //     return true;
        // } else {
        //     alert('Please log in')
        //     this.router.navigate(['']);
        //     return false;
        // }
        if (dayNumber === 5) {
            this.router.navigate(['sunday_evening']);
            return false;
        } else if (dayNumber === 6) {
            // alert('Please log in')
            this.router.navigate(['sunday_evening']);
            return true;
        } else {
            this.router.navigate(['sunday_evening']);
            return false;
        }
    }

}
