import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FullLayoutComponent } from './layouts/full-layout.component';
import { NewsComponent } from './shared/common/news/news.component';
import { NewsItemComponent } from './shared/common/news/news-list/news-item/news-item.component';
import { AuthGuardService } from './services/auth-guard.service';
import { SupercarMainComponent } from './race/supercar/supercar.component';
import { IntermediatePageComponent } from './shared/common/pages/components/intermediate-page.component';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: '',
    component: FullLayoutComponent,
    data: {
      title: 'Home'
    },
    children: [
      // {
      //   path: 'home',
      //   loadChildren: './rxhubmodules/home/home.module#HomeModule',
      //   canActivate: [AuthGuardService]
      // },
      // {
      //   path: 'super1600',
      //   loadChildren: './rxhubmodules/supsix/supsix.module#SupsixModule'
      // },
      // {
      //   path: 'rx2',
      //   loadChildren: './rxhubmodules/rx2/rx2.module#Rx2Module'
      // },
      // {
      //   path: 'supercar_erx',
      //   loadChildren: './rxhubmodules/erx/erx.module#ErxModule'
      // },
      // {
      //   path: 'touringcar',
      //   loadChildren: './rxhubmodules/touringcar/touringcar.module#TouringcarModule'
      // },
      {
        path: 'news',
        component: NewsComponent,
      },
      {
        path: 'news/:id',
        component: NewsItemComponent
      },
      {
        path: 'prerace',
        loadChildren: './rxhubmodules/prerace/prerace.module#PreraceModule'
      },
      {
        path: 'home',
        loadChildren: './rxhubmodules/sunday-evening/sunday-evening.module#SundayEveningModule'
      },
      {
        path: 'supercar',
        component: SupercarMainComponent
      },
      {
        path: 'intermediate_points',
        component: IntermediatePageComponent
      }
      // ,
      // {
      //   path: 'supercar',
      //   loadChildren: './supermodules/supercar/supercar.module#SupercarModule'
      // }
    ]
  }

];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

// export const appRoutingProviders: any[] = [
//
// ];

export class AppRoutingModule {}
// export const routing = RouterModule.forRoot(routes);
