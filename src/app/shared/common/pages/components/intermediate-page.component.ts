import {Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { JsonParseService } from '../../../../services/json-parse.service';
import { IntermediateBaseComponent } from '../../../basecomponent/intermediate-base.component';
import { IntervalObservable } from 'rxjs/observable/IntervalObservable';
import { AnonymousSubscription } from 'rxjs/Subscription';
@Component({
  selector: 'app-intermediate-page',
  templateUrl: '../layouts/intermediate-page.html'
})
export class IntermediatePageComponent extends IntermediateBaseComponent implements OnInit, OnDestroy {
  items: any[] = [];
  catName: any;
  currentParentRoute: string;
  private timerSubscription: AnonymousSubscription;
  private interSubscription: AnonymousSubscription;
  constructor(private service: JsonParseService, private router: Router) {
    super();
    this.router.events.subscribe((res) => {
      this.currentParentRoute = this.router.url.split('/')[2];
      this.catName = this.currentParentRoute;
      this.getAllInters(this.catName );
    })
  }
  getAllInters(catName) {
    this.service.getIntermediatesDupe(catName).subscribe( p => this.items = p );
  }
  ngOnInit() {
    // this.refreshData();
  }
  private refreshData(): void {
    this.interSubscription = this.service.getIntermediates(this.catName)
        .subscribe(
            data => {
              this.items = data;
              this.subscribeToData();
            }
        );
  }
  private subscribeToData(): void {
    this.timerSubscription = IntervalObservable.create(45000).first().subscribe(() => this.refreshData());
  }
  public ngOnDestroy(): void {
    if (this.interSubscription) {
      this.interSubscription.unsubscribe();
    }
    if (this.timerSubscription) {
      this.timerSubscription.unsubscribe();
    }
  }
}
