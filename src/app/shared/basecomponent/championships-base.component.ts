import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { JsonParseService } from '../../services/json-parse.service';
import { environment } from '../../../environments/environment';
@Component({
    moduleId: module.id,
    selector: 'app-championships-base',
    templateUrl: '../../../assets/templates/championships.html',
    styles: []
})
export class ChampionshipsBaseComponent implements OnInit {
    flagsUrl: any = environment.flagsUrl;
    manufacturersUrl: any  = environment.manufacturersUrl;
    carsUrls: any  = environment.carsUrls;
    carSmallSize: any  = environment.carSmallSize;
    carLargeSize: any  = environment.carLargeSize;
    show = 10;
    showMobile = 5;
    next  = 5;
    hide  = true;
    hideMobile  = true;
    items: Array<any>;
    @ViewChild('loadMore') loadMore: ElementRef;
    constructor(public service: JsonParseService) {}

    showMore() {
        let event = new MouseEvent('click', {bubbles: true});
        event.stopPropagation();
        this.show = this.items.length;
        if (this.show >= this.items.length) {
            this.hide = false;
        }
        this.loadMore.nativeElement.dispatchEvent(event);
    }
    showMoreMobile() {
        let event = new MouseEvent('click', {bubbles: true});
        event.stopPropagation();
        this.show = this.items.length;
        if ( this.showMobile >= this.items.length) {
            this.hideMobile = false;
        }
        this.loadMore.nativeElement.click();
    }
    ngOnInit() {}
}
