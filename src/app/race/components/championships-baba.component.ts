import {Component, Inject, OnInit, OnDestroy } from '@angular/core';
import { JsonParseService } from '../../services/json-parse.service';
import { ChampionshipsBaseComponent } from '../../shared/basecomponent/championships-base.component';
import { Router } from '@angular/router';
import { AnonymousSubscription } from 'rxjs/Subscription';
@Component({
    selector: 'app-championships-baba',
    templateUrl: '../../../assets/templates/championships.html'
})
export class ChampionshipsBabaComponent extends ChampionshipsBaseComponent implements OnInit, OnDestroy {
    items: any[] = [];
    catName: any;
    currentParentRoute: string;
    private champsSubscription: AnonymousSubscription;
    constructor(@Inject(JsonParseService) service: JsonParseService, private router: Router) {
        super(service);
        this.router.events.subscribe((res) => {
            this.currentParentRoute = this.router.url.split('/')[2];
            this.catName = this.currentParentRoute;
            this.getAllChamps(this.catName );
        })
    }
    getAllChamps(catName) {
        this.champsSubscription = this.service.getChamps(catName).subscribe( p => this.items = p );
    }
    ngOnInit() {
        // this.refreshData();
    }

    public ngOnDestroy(): void {
        if (this.champsSubscription) {
            this.champsSubscription.unsubscribe();
        }
    }
}
