import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { JsonParseService } from '../../../../services/json-parse.service';
import { environment } from '../../../../../environments/environment';
import { AnonymousSubscription } from 'rxjs/Subscription';
import * as _ from 'lodash';
@Component({
  selector: 'app-entrants-page',
  templateUrl: '../layouts/entrants-page.html'
})
export class EntrantsPageComponent implements OnInit, OnDestroy {
    flagsUrl: any = environment.flagsUrl;
    manufacturersUrl: any  = environment.manufacturersUrl;
    carsUrls: any  = environment.carsUrls;
    carXSmallSize: any  = environment.carSmallSize;
    items: any[] = [];
    counter: any;
    lastItem: number;
    parentRoute: string;
    catName: any;
    private entrantSubscription: AnonymousSubscription;
    constructor(private service: JsonParseService, private router: Router) {
        this.router.events.subscribe((res) => {
            this.parentRoute = this.router.url.split('/')[2];
            this.catName = this.parentRoute;
            this.getEntrantsPageDup(this.catName);
        });
    }
    getEntrantsPageDup(catName) {
        this.service.getEntrantsPageDup(catName)
            .subscribe(
                data => {
                    this.items = data;
                }
            );
    }
    getEntrantsPage(catName) {
        this.entrantSubscription = this.service.getEntrantsPage(catName)
          .subscribe(
              data => {
                  this.items = data;
              }
          );
    }
    countEntrants(catName) {
        this.entrantSubscription = this.service.countEntrants(catName)
            .subscribe( data => {
                this.counter = data;
                this.lastItem = 0;
                this.lastItem = _.parseInt(_.size(this.items));
                this.counter = _.parseInt(this.counter);
                if (this.lastItem < this.counter) {
                    this.getEntrantsPageDup(catName);
                }else {
                    this.items = this.items;
                }
                // console.log(this.counter);
                // console.log(this.lastItem);
            });
    }
    ngOnInit() {
        // this.getEntrantsPage(this.catName);
        this.countEntrants(this.catName);
    }
    public ngOnDestroy(): void {
        if (this.entrantSubscription) {
          this.entrantSubscription.unsubscribe();
        }
    }

}
