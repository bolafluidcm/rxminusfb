import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LiveTimingsComponent } from './live-timings.component';

describe('LiveTimingsComponent', () => {
  let component: LiveTimingsComponent;
  let fixture: ComponentFixture<LiveTimingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LiveTimingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LiveTimingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
