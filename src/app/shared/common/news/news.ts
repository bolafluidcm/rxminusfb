export interface News {
    id: string;
    title: string;
    content: string;
    image: string;
    url: string;
  // constructor(
  //  public id: number,
  //  public title: string,
  //  public content: string,
  //  public image: string,
  //  public url: string,
  // ){}
}
