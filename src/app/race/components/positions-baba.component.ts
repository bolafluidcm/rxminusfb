import { Component, ElementRef, ViewChild, Inject, OnInit, OnDestroy } from '@angular/core';
import { JsonParseService } from '../../services/json-parse.service';
import { Router } from '@angular/router';
import _ from 'lodash';
import { PositionsBaseComponent } from '../../shared/basecomponent/positions-base.component';
import { AnonymousSubscription } from 'rxjs/Subscription';
@Component({
    selector: 'app-positions-baba',
    templateUrl: '../../../assets/templates/positions.html'
})
export class PositionsBabaComponent extends PositionsBaseComponent implements OnInit, OnDestroy {
    private interSubscription: AnonymousSubscription;
    currentParentRoute: string;
    catName: any;
    config: SwiperOptions = {
        pagination: '.swiper-pagination',
        paginationClickable: true,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        spaceBetween: 10,
        slidesPerView: 1,
        loop: false,
        breakpoints: {
            320: {
                slidesPerView: 1,
                spaceBetween: 0
            },
            480: {
                slidesPerView: 1,
                spaceBetween: 0
            },
            640: {
                slidesPerView: 1,
                spaceBetween: 30
            }
        }
    };
    @ViewChild('loadMore') loadMore: ElementRef;
    constructor(@Inject(JsonParseService) service: JsonParseService, private router: Router) {
        super( service );
        this.router.events.subscribe((res) => {
            this.currentParentRoute = this.router.url.split('/')[2];
            this.catName = this.currentParentRoute;
            this.getAllPositionsDupe(this.catName );
        })
    }
    getAllPositionsDupe(catName) {
        this.service.getPositionsDupe(catName).subscribe(
            data => {
                const grouped = _.groupBy(data, function (d) {
                    return  d.heat;
                });
                const ordered = {};
                Object.keys(grouped).sort().forEach(function(key) {
                    ordered[key] = grouped[key];
                });
                this.items = ordered;
            });
    }
    getAllPositions(catName) {
        this.interSubscription = this.service.getPositions(catName).subscribe(
            data => {
                const grouped = _.groupBy(data, function (d) {
                    return  d.heat;
                });
                const ordered = {};
                Object.keys(grouped).sort().forEach(function(key) {
                    ordered[key] = grouped[key];
                });
                this.items = ordered;
            });
    }
    ngOnInit() {
        this.getAllPositions(this.catName );
    }
    public ngOnDestroy(): void {
        if (this.interSubscription) {
            this.interSubscription.unsubscribe();
        }
    }
}
