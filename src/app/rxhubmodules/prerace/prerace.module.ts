import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PreraceRoutingModule } from './prerace-routing.module';
// import { PreraceNewsListComponent } from './prerace-news-list.component';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    PreraceRoutingModule,
    SharedModule    
  ],
  declarations: [ ]
})
export class PreraceModule { }
