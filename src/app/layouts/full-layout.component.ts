import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Dropdown } from './dropdown';
import { Sidenav } from './sidenav';
import { JsonParseService } from '../services/json-parse.service'
import { environment } from '../../environments/environment';
@Component({
  selector: 'app-dashboard',
  templateUrl: './../../assets/templates/full-layout.html'
})
export class FullLayoutComponent implements OnInit {
  public rx2home = environment.rx2Home;
  public disabled = false;
  // public status: {isopen: boolean} = {isopen: false};
  errorMessage: string;
  currentEvent:  any[];
  // currentEventClass:  any[] = [];
  selectedDropdown: Dropdown;
  dropdowns = [
  new Dropdown(1, 'SC', 'Supercar', 'supercar'),
  new Dropdown(2, 'S1600', 'Super1600', 'super1600'),
  new Dropdown(3, 'TC', 'Touringcar', 'touringcar'),
  new Dropdown(4, 'RX2', 'RX2', 'rx2'),
  new Dropdown(5, 'ERX', 'Supercar ERX', 'supercar_erx')
  ];
  sidenavs = [
  new Sidenav('Schedule of events', 'schedule_of_events'),
  new Sidenav('Entrants', 'entrants'),
  new Sidenav('Live timings', 'live_timings'),
  new Sidenav('Intermediate points', 'intermediate_points'),
  new Sidenav('Championship points', 'championship_points'),
  new Sidenav('Race documents', 'race_documents')
  ];
  activeRoute: any;
  toSplitRoute: any;
  parentRoute: string;
  currentParentRoute: string;

  constructor(private router: Router, private service: JsonParseService) {
    this.router.events.subscribe((res) => {
      this.activeRoute = this.router.url;
      this.toSplitRoute = this.router.url.split(/[/ ]+/).shift();
      this.parentRoute = this.router.url.split('/')[1];
      this.currentParentRoute = this.router.url.split('/')[2];
      // this.getEventClass();
    })
    // ! !both methods working
    // router.events.subscribe(event => {
    //   if (event instanceof NavigationEnd) {
    //     this.activeRoute = event.url;
    //     console.log(this.activeRoute ,"Current URL");
    //   }
    // });

  }

  ngOnInit(): void {
    this.selectedDropdown = this.dropdowns[0];
    this.getEvent();
  }
  getEvent() {
  this.service.getEvent()
      .subscribe(
          currentEvent => this.currentEvent = currentEvent,
          error =>  this.errorMessage = <any>error);
  }
  // getEventClass() {
  //   this.service.getEventClass()
  //     .subscribe(
  //     data => {
  //       this.currentEventClass = data;
  //     });
  // }
  public toggled(open: boolean): void {
    console.log('Dropdown is now: ', open);
  }
  // public toggleDropdown($event: MouseEvent): void {
  //   $event.preventDefault();
  //   $event.stopPropagation();
  //   this.status.isopen = !this.status.isopen;
  // }
}
