import {Component, Inject, OnInit, OnDestroy } from '@angular/core';
import { JsonParseService } from '../../services/json-parse.service';
import * as _ from 'lodash';
import {HeatBaseComponent} from '../../shared/basecomponent/heat-base.component';
import {Router} from '@angular/router';
import {AnonymousSubscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-heat-baba',
  templateUrl: '../../../assets/templates/supercarheats.html'
})
export class HeatBabaComponent extends HeatBaseComponent implements OnInit, OnDestroy {
  items: any[] = [];
  catName: any;
  parentRoute: string;
  currentParentRoute: string;
  private heatsSubscription: AnonymousSubscription;
  constructor(@Inject(JsonParseService) service: JsonParseService, private router: Router) {
    super(service);
    this.router.events.subscribe((res) => {
      this.currentParentRoute = this.router.url.split('/')[2];
      this.catName = this.currentParentRoute;
      this.getAllHeatsDupe(this.catName );
    })
  }
  getAllHeatsDupe(catName) {
    this.service.getAllHeatsDupe(catName).subscribe(
      data => {
        const grouped = _.groupBy(data, function (d) {
          return d.heat + '-' + d.Race_Number;
        });
        this.items = grouped;
        const lastItem = _.size(this.items) - 1;
        this.usefulSwiper.swiper.slideTo(lastItem);
        this.usefulSwiperMobile.swiper.slideTo(lastItem);
      });
  }
  getAllHeats(catName) {
      this.heatsSubscription = this.service.getAllHeats(catName).subscribe(
      data => {
        const grouped = _.groupBy(data, function (d) {
          return d.heat + '-' + d.Race_Number;
        });
        this.items = grouped;
        const lastItem = _.size(this.items) - 1;
        this.usefulSwiper.swiper.slideTo(lastItem);
        this.usefulSwiperMobile.swiper.slideTo(lastItem);
      });
  }
  ngOnInit() {
    this.getAllHeats(this.catName);
  }

  public ngOnDestroy(): void {
    if (this.heatsSubscription) {
      this.heatsSubscription.unsubscribe();
    }
  }
}
