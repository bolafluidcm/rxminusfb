import { Component, OnInit } from '@angular/core';
import { Routes, RouterModule, Router, ActivatedRoute } from '@angular/router';
import { NewsService } from './news.service';


@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styles: []
})
export class NewsComponent implements OnInit {
  errorMessage: string;
  news: any[];

  constructor( private newsService: NewsService, private _router: Router ) { }

  ngOnInit() {
    this.getCurrentNews();
  }

  getCurrentNews() {
    this.newsService.getCurrentNews()
      .subscribe(
        news => this.news = news,
        error =>  this.errorMessage = <any>error);
  }

  showNewsItemDetail(newsItem){
    this._router.navigate(['/news', newsItem.id]);
  }

}
