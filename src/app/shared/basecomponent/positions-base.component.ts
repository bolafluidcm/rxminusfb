import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { JsonParseService } from '../../services/json-parse.service';
import { environment } from '../../../environments/environment';

@Component({
  moduleId: module.id,
  selector: 'app-positions-base',
  templateUrl: '../../../assets/templates/positions.html'
})

export class PositionsBaseComponent implements OnInit {
  public erx = environment.erx;
  flagsUrl: any = environment.flagsUrl;
  manufacturersUrl: any  = environment.manufacturersUrl;
  carsUrls: any  = environment.carsUrls;
  carSmallSize: any  = environment.carSmallSize;
  carLargeSize: any  = environment.carLargeSize;
  show = 10;
  showMobile = 5;
  next = 5;
  hide = true;
  items: any;

  config: SwiperOptions = {
    pagination: '.swiper-pagination',
    paginationClickable: true,
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    spaceBetween: 10,
    slidesPerView: 1,
    loop: false
  };
  @ViewChild('loadMore') loadMore: ElementRef;
  constructor(public service: JsonParseService) {
    // const catID = this.erx;
    // service.getPositions(catID).subscribe(
    //   data => {
    //     const grouped = _.groupBy(data, function (d) {
    //       return  d.heat;
    //     });
    //     this.items = grouped;
    //   });
  }

  showMore() {
    const event = new MouseEvent('click', {bubbles: true});
    event.stopPropagation();
    this.show = this.show + this.next;
    if (this.show > this.items.length) {
      this.hide = false;
    }
    this.loadMore.nativeElement.click();
  }

  showMoreMobile() {
    const event = new MouseEvent('click', {bubbles: true});
    event.stopPropagation();
    this.showMobile = this.showMobile + this.next;
    if (this.showMobile > this.items.length) {
      this.hide = false;
    }
    this.loadMore.nativeElement.click();
  }
  ngOnInit() {}

}
